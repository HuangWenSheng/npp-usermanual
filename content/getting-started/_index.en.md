---
title: Getting started
linktitle: Getting started
weight: 1
---

## What is Notepad++
Notepad++ is a text editor and source code editor for use under MS Windows. It supports around 80 programming lanuages with syntax highlighting, code folding. It allows working with multiple open files in a single window, thanks its tabbed editing interface.
Notepad++ is under [GPL](http://www.gnu.org/licenses/gpl-3.0.html), distributed as [free software](https://www.fsf.org/).


## Download Notepad++
Download the latest version of Notepad++ from the following link:
https://notepad-plus-plus.org/download/

Choose 32 or 64 bits Notepad++ build according your Operation System. Then choose the package you want to download. Users use installer normally for its simplicity. Some users might want to unzip the binary in a specific directory - download zip/7z packages in this case.


## Install Notepad++
If you've downloaded installer, just double click on the excutable binary, then click on `Next` and `Next` till the end.
Whereas using the zip/7z packages is simple as well. All you need is create a new folder and unzip the content into this empty folder.    
